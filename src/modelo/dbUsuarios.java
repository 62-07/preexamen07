/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modelo;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class dbUsuarios extends dbManejador implements persistencia{

    @Override
    public boolean insertar(Object objecto) throws Exception {
        Usuarios usu = new Usuarios();
        usu=(Usuarios) objecto;
        String consulta="insert into usuarios(nombre,correo,contraseña) values(?,?,?)";
        
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta=conexion.prepareStatement(consulta);
                //INGRESAR VALORES A LA CONSULTA
                this.sqlConsulta.setString(1,usu.getNombre());
                this.sqlConsulta.setString(2,usu.getCorreo());
                this.sqlConsulta.setString(3,usu.getContraseña());
                
                this.sqlConsulta.executeUpdate();
                System.out.println("Se agrego exitosamente");
                return true;
            } catch (SQLException e) {
                System.out.println("Surgio un error al insertar "+e.getMessage());
            }            
        }
        else{
            System.out.println("No fue posible concectarse");
        }
        this.desconectar();
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Usuarios usu = new Usuarios();
        usu=(Usuarios) objecto;
        String consulta = "update usuarios set nombre = ?, correo= ?,contraseña=? where nombre = ?";
        
        if(this.conectar()){
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // ASIGNAR VALORES DEL CAMPO 
                
                this.sqlConsulta.setString(1,usu.getNombre());
                this.sqlConsulta.setString(2,usu.getCorreo());
                this.sqlConsulta.setString(3,usu.getContraseña()); 
                
                this.sqlConsulta.executeUpdate();
                
                System.out.println("Se actualizo");
                
            } catch (Exception e) {
                System.out.println("Surgio un error al actualizar "+e.getMessage());
            }
            this.desconectar();
        }
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean borrar(Object objecto, String codigo) throws Exception {
        Usuarios usu = new Usuarios();
        
        usu=(Usuarios) objecto;
        String consulta = "delete from usuarios where nombre = ?";
        
        if(this.conectar()){
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // ASIGNAR VALORES DEL CAMPO 
                this.sqlConsulta.setString(1,usu.getNombre()); 

                this.sqlConsulta.executeUpdate();
                this.desconectar();
                return true;
                
            } catch (Exception e) {
                System.out.println("Surgio un error al eliminar "+e.getMessage());
            }
            this.desconectar();
        }
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean isExiste(String nombre) throws Exception {
       Usuarios usu = new Usuarios();
       if(this.conectar()){
            String consulta ="Select * from usuarios where nombre=?";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, nombre);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                this.desconectar();
                return true;
            }
       }this.desconectar();
       return false;        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Usuarios> lista = new ArrayList<Usuarios>();
        Usuarios usu;

        if(this.conectar()){
            String consulta = "SELECT * from usuarios ORDER BY nombre";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();

            while(this.registros.next()){
                usu = new Usuarios();

                usu.setIdUsuarios(this.registros.getInt("idUsuarios"));
                usu.setNombre(this.registros.getString("nombre"));
                usu.setCorreo(this.registros.getString("correo"));
                usu.setContraseña(this.registros.getString("contraseña"));
                
                lista.add(usu);
            }
        }
        this.desconectar();
        return lista;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(String nombre) throws Exception {
       Usuarios usu = new Usuarios();
       if(this.conectar()){
            String consulta ="Select * from usuarios where nombre = ?";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, nombre);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                //usu.setIdUsuarios(this.registros.getInt("idUsuarios"));
                usu.setNombre(this.registros.getString("nombre"));
                //usu.setCorreo(this.registros.getString("correo"));
                usu.setContraseña(this.registros.getString("contraseña"));
                
              //  usu.setStatus(this.registros.getInt("status"));
            }
            
       }this.desconectar();
       
       return usu;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
