/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package modelo;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public interface persistencia {
    public boolean insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public boolean borrar(Object objecto,String codigo) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar(String criterio) throws Exception ;
   
    public Object buscar(String codigo) throws Exception;
    
}
